#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  // for diffuse directional light shader
  const auto lightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto lightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uLightIntensity");

  // for pbr directional light shader
  const auto baseColorTextureLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto baseColorFactorLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
  const auto metallicFactorLocation = glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto roughnessFactorLocation = glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto metallicRoughnessLocation = glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto emissiveFactorLocation = glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto emissiveTextureLocation = glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto occlusionStrengthLocation = glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto occlusionTextureLocation = glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto applyOcclusionLocation = glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");

  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(1, 1, 1);


  tinygltf::Model model;
  // TODO Loading the glTF file
  if(loadGltFile(model) == false) {
    return -1;
  }

  glm::vec3 bboxMin;
  glm::vec3 bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);
  const auto diag = bboxMax - bboxMin;
  
  static bool lightFromCamera = false;
  bool applyOcclusion = true;

  // Build projection matrix
  auto maxDistance = glm::length(diag); // TODO use scene bounds instead to compute this
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(
      m_GLFWHandle.window(), 0.5f * maxDistance);
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    const auto center = (bboxMin + bboxMax)*0.5f;
    const auto up = glm::vec3(0, 1, 0);
    const auto eye = diag.z > 0 ? center + diag : center + 2.f*glm::cross(diag, up);
    cameraController->setCamera(
        Camera{eye, center, up});
  }

  // Creation of Texture Objects
  const auto textureObjects = createTextureObjects(model);

  // Default white texture
  GLuint whiteTexture = 0;
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  float white[] = {1, 1, 1, 1};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  // TODO Creation of Buffer Objects
  const auto bufferObjects = createBufferObjects(model);

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  const auto vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();


  // Lambda function to bind texture
  const auto bindMaterial = [&](const auto materialIndex) {
    // Material binding
    if(materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      if(baseColorFactorLocation >= 0) {
        glUniform4f(baseColorFactorLocation,
              (float)pbrMetallicRoughness.baseColorFactor[0],
              (float)pbrMetallicRoughness.baseColorFactor[1],
              (float)pbrMetallicRoughness.baseColorFactor[2],
              (float)pbrMetallicRoughness.baseColorFactor[3]);
      }
      if(baseColorTextureLocation >= 0) {
        auto textureObject = whiteTexture;
        if(pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if(texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(baseColorTextureLocation, 0);
      }
      if (metallicFactorLocation >= 0) {
        glUniform1f(
            metallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
      }
      if (roughnessFactorLocation >= 0) {
        glUniform1f(
            roughnessFactorLocation, (float)pbrMetallicRoughness.roughnessFactor);
      }
      if (metallicRoughnessLocation >= 0) {
        auto textureObject = 0u;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture
                                 .index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(metallicRoughnessLocation, 1);
      }
      if (emissiveFactorLocation >= 0) {
        glUniform3f(emissiveFactorLocation, (float)material.emissiveFactor[0],
            (float)material.emissiveFactor[1],
            (float)material.emissiveFactor[2]);
      }
      if (emissiveTextureLocation >= 0) {
        auto textureObject = 0u;
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(emissiveTextureLocation, 2);
      }
      if (occlusionStrengthLocation >= 0) {
        glUniform1f(
            occlusionStrengthLocation, (float)material.occlusionTexture.strength);
      }
      if (occlusionTextureLocation >= 0) {
        auto textureObject = whiteTexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(occlusionTextureLocation, 3);
      }



    }

    else {
      if(baseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(baseColorTextureLocation, 0);
      }
      if(baseColorFactorLocation >= 0) {
        glUniform4f(baseColorFactorLocation, 1, 1, 1, 1);
      }
      if (metallicFactorLocation >= 0) {
        glUniform1f(metallicFactorLocation, 1.f);
      }
      if (roughnessFactorLocation >= 0) {
        glUniform1f(roughnessFactorLocation, 1.f);
      }
      if (metallicRoughnessLocation >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(metallicRoughnessLocation, 1);
      }
      if (emissiveFactorLocation >= 0) {
        glUniform3f(emissiveFactorLocation, 0.f, 0.f, 0.f);
      }
      if (emissiveTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(emissiveTextureLocation, 2);
      }
      if (occlusionStrengthLocation >= 0) {
        glUniform1f(occlusionStrengthLocation, 0.f);
      }
      if (occlusionTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(occlusionTextureLocation, 3);
      }



    }
  };



  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    if(lightIntensityLocation >= 0) {
      glUniform3f(lightIntensityLocation, lightIntensity.x, lightIntensity.y, lightIntensity.z);
    }
    if (applyOcclusionLocation >= 0) {
      glUniform1i(applyOcclusionLocation, applyOcclusion);
    }

    
    if(lightDirectionLocation >= 0) {
      if(lightFromCamera) {
        glUniform3f(lightDirectionLocation, 0, 0, 1);
      }
      else {
        const auto lightDirectionView = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0)));
        glUniform3f(lightDirectionLocation, lightDirectionView.x, lightDirectionView.y, lightDirectionView.z);
      }
      
    }
    

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // TODO The drawNode function
          const auto node = model.nodes[nodeIdx];
          const glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if(node.mesh >= 0) {
            const auto modelViewMatrix = viewMatrix * modelMatrix;
            const auto modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            const auto normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

            const auto &mesh = model.meshes[node.mesh];
            const auto &VAORange = meshIndexToVaoRange[node.mesh];
            for(size_t primitiveIdx = 0; primitiveIdx < mesh.primitives.size(); ++primitiveIdx) {
              const auto &primitive = mesh.primitives[primitiveIdx];

              bindMaterial(primitive.material);

              const auto VAO = vertexArrayObjects[VAORange.begin + primitiveIdx];
              glBindVertexArray(VAO);              

              if (primitive.indices >= 0) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
                glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const GLvoid*)byteOffset);
              }
              else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];  
                glDrawArrays(primitive.mode, 0, accessor.count);
              }
            }
          }
          for(const auto child : node.children) {
            drawNode(child, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // TODO Draw all nodes
      for(const auto node : model.scenes[model.defaultScene].nodes) {
        drawNode(node, glm::mat4(1));
      }
    }
  };

    if(!m_OutputPath.empty()) {
    std::vector<unsigned char> pixels(m_nWindowWidth*m_nWindowHeight*3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() {
      drawScene(cameraController->getCamera());
    });
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());
    const auto strPath = m_OutputPath.string();
    stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        static int cameraControllerType = 0;
        const auto cameraControllerTypeChanged =
            ImGui::RadioButton("Trackball", &cameraControllerType, 0) ||
            ImGui::RadioButton("First Person", &cameraControllerType, 1);
        if (cameraControllerTypeChanged) {
          const auto currentCamera = cameraController->getCamera();
          if (cameraControllerType == 0) {
            cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          else {
            cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController->setCamera(currentCamera);
        }
      }

      //Light
      if(ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        //light direction
        static float theta = 0.f;
        static float phi = 0.f;
        if(ImGui::SliderFloat("theta", &theta, 0, M_PI) || ImGui::SliderFloat("phi", &phi, 0, M_PI * 2)) {
          lightDirection = glm::vec3(sin(theta)*cos(phi), cos(theta), sin(theta)*sin(phi));
        }
        
        //light intensity
        static glm::vec3 color(1, 1, 1);
        static float intensityFactor = 1;
        if(ImGui::ColorEdit3("color", (float*)&color) || ImGui::InputFloat("intensity", &intensityFactor)) {
          lightIntensity = color * intensityFactor;
        };

        //light from camera
        ImGui::Checkbox("ligth from camera", &lightFromCamera);
        //Occlusion
        ImGui::Checkbox("apply occlusion", &applyOcclusion);
      }

      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}

bool ViewerApplication::loadGltFile(tinygltf::Model & model) {
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return false;
  }
  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(const tinygltf::Model &model) {
  std::vector<GLuint> bufferVector(model.buffers.size(), 0);
  glGenBuffers(model.buffers.size(), bufferVector.data());
  for(size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferVector[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return bufferVector;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects( const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> & meshIndexToVaoRange) {
  std::vector<GLuint> vertexArrayObjects;
  for(size_t meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx) {
    const auto vaoOffset = vertexArrayObjects.size();
    vertexArrayObjects.resize(vaoOffset + model.meshes[meshIdx].primitives.size());
    meshIndexToVaoRange.push_back(VaoRange{(GLsizei)vaoOffset, (GLsizei)model.meshes[meshIdx].primitives.size()}); // Will be used during rendering

    glGenVertexArrays(model.meshes[meshIdx].primitives.size(), &vertexArrayObjects[vaoOffset]);

    for(size_t primitiveIdx = 0; primitiveIdx < model.meshes[meshIdx].primitives.size(); ++primitiveIdx) {
      glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);
      const auto primitive = model.meshes[meshIdx].primitives[primitiveIdx];
  
      { // I'm opening a scope because I want to reuse the variable iterator in the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

          const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = bufferView.byteOffset + accessor.byteOffset;// TODO Compute the total byte offset using the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type, accessor.componentType, GL_FALSE, bufferView.byteStride, (const GLvoid*)byteOffset);
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }

      { // Some for NORMAL 
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != end(primitive.attributes)) {
          
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; 
          const auto &bufferView = model.bufferViews[accessor.bufferView]; 
          const auto bufferIdx = bufferView.buffer; 

          const auto bufferObject = bufferObjects[bufferIdx]; 

          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = bufferView.byteOffset + accessor.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type, accessor.componentType, GL_FALSE, bufferView.byteStride, (const GLvoid*)byteOffset);
        }
      }

      { // Some for TEXCOORD_0
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != end(primitive.attributes)) {
          
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; 
          const auto &bufferView = model.bufferViews[accessor.bufferView]; 
          const auto bufferIdx = bufferView.buffer; 

          const auto bufferObject = bufferObjects[bufferIdx]; 

          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = bufferView.byteOffset + accessor.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, accessor.componentType, GL_FALSE, bufferView.byteStride, (const GLvoid*)byteOffset);
        }
      }

      if(primitive.indices >= 0) {
        const auto &accessor = model.accessors[primitive.indices]; 
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer; 
        const auto bufferObject = bufferObjects[bufferIdx];

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject);
      }

    }
  }
  // End the description of our vertex array object:
  glBindVertexArray(0);

  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const {
  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  // Default sampler
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);
  // Generate texture object
  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());

  for(size_t texIdx = 0; texIdx < model.textures.size(); ++texIdx) {
    const auto &texture = model.textures[texIdx];
    assert(texture.source >= 0);
    const auto &image = model.images[texture.source];

    // Bind texture object to target GL_TEXTURE_2D
    glBindTexture(GL_TEXTURE_2D, textureObjects[texIdx]);

    // Set image data
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());

    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    
    // Set sampling parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    // Set wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
  }

  return textureObjects;
}