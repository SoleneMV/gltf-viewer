#version 330

in vec3 vViewSpaceNormal;
in vec2 vTexCoords;
in vec3 vViewSpacePosition;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

uniform sampler2D uBaseColorTexture;
uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

uniform vec3 uEmissiveFactor;
uniform sampler2D uEmissiveTexture;

uniform float uOcclusionStrength;
uniform sampler2D uOcclusionTexture;
uniform int uApplyOcclusion;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
    return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main()
{
    vec3 N = normalize(vViewSpaceNormal);
    vec3 L = uLightDirection;
    vec3 V = normalize(-vViewSpacePosition);
    vec3 H = normalize(L + V);

    vec4 baseColorFromTexture =
      SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
    vec4 baseColor = uBaseColorFactor * baseColorFromTexture;

    vec4 metallicRougnessFromTexture =
      texture(uMetallicRoughnessTexture, vTexCoords);
    vec3 metallic = vec3(uMetallicFactor * metallicRougnessFromTexture.b);
    float roughness = uRoughnessFactor * metallicRougnessFromTexture.g;


    float alpha = roughness*roughness;
    float alpha2 = alpha*alpha;
    float NdotH = clamp(dot(N, H), 0., 1.);
    float NdotL = clamp(dot(N, L), 0., 1.);
    float NdotV = clamp(dot(N, V), 0., 1.);
    float HdotL = clamp(dot(H, L), 0., 1.);
    float HdotV = clamp(dot(H, V), 0., 1.);
    float VdotH = clamp(dot(V, H), 0., 1.);

    vec3 dielectricSpecular = vec3(0.04);
    vec3 black = vec3(0.);
    vec3 c_diff = mix(baseColor.rgb * (1 - dielectricSpecular.r), black, metallic);
    vec3 F_0 = mix(vec3(dielectricSpecular), baseColor.rgb, metallic);
    float baseShlickFactor = 1 - VdotH ;
    // You need to compute baseShlickFactor first
    float shlickFactor = baseShlickFactor * baseShlickFactor; // power 2
    shlickFactor *= shlickFactor; // power 4
    shlickFactor *= baseShlickFactor; // power 5
    vec3 F = F_0 + (1 - F_0) * shlickFactor;

    vec3 f_diffuse = (1 - F) * (1 / M_PI) * c_diff;

    float VisFD = NdotL + sqrt(alpha2 + (1-alpha2) * NdotL*NdotL);
    float VisSD = NdotV + sqrt(alpha2 + (1-alpha2) * NdotV*NdotV);
    float VisFP = HdotL > 0 ? 1 / VisFD : 0;
    float VisSP = HdotV > 0 ? 1 / VisSD : 0;
    float Vis = VisFD * VisSD > 0 ? VisFP * VisSP : 0;

    float DPartialDenom = (NdotH * NdotH * (alpha2 - 1) + 1);
    float D = NdotH > 0 ? alpha2 / (M_PI * DPartialDenom * DPartialDenom) : 0;

    vec3 f_specular = F * Vis * D;

    vec3 material = f_diffuse + f_specular;

    vec3 emissive = SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;

    vec3 color = material * uLightIntensity * NdotL + emissive;

    if (1 == uApplyOcclusion) {
      float ao = texture2D(uOcclusionTexture, vTexCoords).r;
      color = mix(color, color * ao, uOcclusionStrength);
    }
    
    fColor = LINEARtoSRGB(color);
}